using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Samples.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using eBay.Service.EPS;
using System.Drawing.Imaging;

namespace asos_monitor
{
    class AddProducts
    {
        readonly Tgram tele = Form1.tele;
        readonly int site = 1;
        readonly ApiContext Context;
        JObject jObj;
        readonly string country = "AU";
        readonly string currency = "AUD";
        readonly string lang = "en-AU";
        double price;
        readonly double ptofit = 5;
        string category;
        int sku;
        string description;
        readonly string[] defaulttext = { "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><font rwr=\"1\" size=\"4\" style=\"font-family:Arial\"><div style=\"margin: 0px; padding: 0px 10px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: futura-pt-n4, futura-pt, Tahoma, Geneva, Verdana, Arial, sans-serif; vertical-align: baseline; float: left; min-height: 1px; box-sizing: border-box; width: 319.859px; color: rgb(45, 45, 45); letter-spacing: 0.4px;\"><div style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><h4 style=\"margin: 20px 0px 5px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.7; font-family: futura-pt-n7, futura-pt, Tahoma, Geneva, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(153, 153, 153); letter-spacing: 2px; text-transform: uppercase;\"> PRODUCT DETAILS</h4><span style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\">",
                                 "</span></div></div><div style=\"margin: 0px; padding: 0px 9px 0px 10px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: futura-pt-n4, futura-pt, Tahoma, Geneva, Verdana, Arial, sans-serif; vertical-align: baseline; float: left; min-height: 1px; box-sizing: border-box; width: 319.859px; color: rgb(45, 45, 45); letter-spacing: 0.4px;\" ><div style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; clear: both;\"><h4 style=\"margin: 20px 0px 5px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.7; font-family: futura-pt-n7, futura-pt, Tahoma, Geneva, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(153, 153, 153); letter-spacing: 2px; text-transform: uppercase;\">BRAND</h4></div><div style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><span style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\">",
                                 "</span></div></div><div style=\"margin: 0px; padding: 0px 10px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: futura-pt-n4, futura-pt, Tahoma, Geneva, Verdana, Arial, sans-serif; vertical-align: baseline; float: left; min-height: 1px; box-sizing: border-box; width: 319.859px; color: rgb(45, 45, 45); letter-spacing: 0.4px;\"><div style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><h4 style=\"margin: 20px 0px 5px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.7; font-family: futura-pt-n7, futura-pt, Tahoma, Geneva, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(153, 153, 153); letter-spacing: 2px; text-transform: uppercase;\">SIZE &amp; FIT</h4><span style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\">",
                                 "</span></div><div style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><h4 style=\"margin: 20px 0px 5px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.7; font-family: futura-pt-n7, futura-pt, Tahoma, Geneva, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(153, 153, 153); letter-spacing: 2px; text-transform: uppercase;\">LOOK AFTER ME</h4><span style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\">",
                                 "</span></div><div style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><h4 style=\"margin: 20px 0px 5px; padding: 0px; border: 0px; font-variant: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.7; font-family: futura-pt-n7, futura-pt, Tahoma, Geneva, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(153, 153, 153); letter-spacing: 2px; text-transform: uppercase;\">ABOUT ME</h4><span style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\">",
                                 "</span></div></div></font>" };
        string[] picURLs;

        public AddProducts()
        {
            EbayApi(ref Context);
        }

        private void AddProduct(int sk)
        {
            sku = sk;
            jObj = AsosConnect(sku);
            price = (Convert.ToDouble(jObj["price"]["current"]["value"]) / 0.8) + (5-(Convert.ToDouble(jObj["price"]["current"]["value"]) / 0.8)%5) + ptofit;
            GetPhoto();
            GetDescription();
            Getcategory();            
            AddFixedPriceItemCall AddFPItemCall = new AddFixedPriceItemCall(Context);
            ItemType item = new ItemType
            {
                Title = Convert.ToString(jObj["name"]),
                ConditionID = 1000,
                Description = description,
                Country = CountryCodeType.GB,
                Currency = CurrencyCodeType.AUD,
                SKU = Convert.ToString(sku),
                ListingDuration = "GTC",
                Location = "leeds",
                PrimaryCategory = new CategoryType
                {
                    CategoryID = category
                },
                PictureDetails = new PictureDetailsType
                {
                    PictureURL = new StringCollection {
                    picURLs[0],picURLs[1],picURLs[2],picURLs[3]
                },
                    GalleryType = GalleryTypeCodeType.Plus,
                    PictureSource = PictureSourceCodeType.EPS

                },
                ItemSpecifics = new NameValueListTypeCollection
                {
                    new NameValueListType
                    {
                        Name = "Brand",
                        Value = new StringCollection {
                            Convert.ToString(jObj["brand"]["name"])
                        }
                    },
                    new NameValueListType
                    {
                        Name = "Colour",
                        Value = new StringCollection {
                            Convert.ToString(jObj["variants"][0]["colour"])
                        }
                    }
                },
                SellerProfiles = new SellerProfilesType
                {
                    SellerPaymentProfile = new SellerPaymentProfileType
                    {
                        PaymentProfileID = 159136024021,
                        PaymentProfileName = "PayPal:Immediate pay"
                    },
                    SellerReturnProfile = new SellerReturnProfileType
                    {
                        ReturnProfileID = 159138161021,
                        ReturnProfileName = "Returns Accepted,Seller,30 Days,International#1"
                    },
                    SellerShippingProfile = new SellerShippingProfileType
                    {
                        ShippingProfileID = 159138166021,
                        ShippingProfileName = "Flat:Expedited deli(Free),2 business days"
                    }
                },
            };
            
            var result = jObj["variants"].Select(iem => new
            {
                Size = iem["brandSize"],
                isInStock = iem["isInStock"]
            }).ToList();

            if (result.Count != 1)
            {
                int quan = 1;
                item.Variations = new VariationsType
                {
                    VariationSpecificsSet = new NameValueListTypeCollection()
                    
                };
                NameValueListType NVListVS1 = new NameValueListType
                {
                    Name = "Size"
                };
                StringCollection VSvaluecollection1 = new StringCollection();
                VariationTypeCollection VarCol = new VariationTypeCollection();

                for (int i = 0; i < result.Count; i++)
                {
                    if (Convert.ToString(result[i].isInStock).ToLower() == "false") quan = 0;
                    VariationType var1 = new VariationType
                    {
                        SKU = Convert.ToString(result[i].Size),
                        
                        VariationProductListingDetails = new VariationProductListingDetailsType { UPC = "Does not apply" },
                        Quantity = quan,
                        StartPrice = new AmountType
                        {
                            currencyID = CurrencyCodeType.AUD,
                            Value = price
                        },
                        VariationSpecifics = new NameValueListTypeCollection
                        {
                            new NameValueListType
                            {
                                Name = "Size",
                                Value = new StringCollection
                                {
                                    Convert.ToString(result[i].Size)
                                }
                            }
                        }
                    };
                    NameValueListType Var1Spec1 = new NameValueListType();
                    VSvaluecollection1.Add(Convert.ToString(result[i].Size));
                    VarCol.Add(var1);
                }
                NVListVS1.Value = VSvaluecollection1;
                item.Variations.VariationSpecificsSet.Add(NVListVS1);
                item.Variations.Variation = VarCol;
            }
            else
            {
                item.ProductListingDetails = new ProductListingDetailsType
                {
                    UPC = "Does not apply"
                };

                item.Quantity = 1;
                item.StartPrice = new AmountType
                {
                    currencyID = CurrencyCodeType.AUD,
                    Value = price,
                };
            }

            AddFPItemCall.Item = item;
            try
            {
                AddFPItemCall.Execute();
            }
            catch (Exception ex)

            {
                tele.Telereport(ex.Message);
            }
        }

        public void ArrProducts(List<int> prods)
        {
            if (prods == null) return;
            for (int i = 0; i < prods.Count; i++)
            {
                AddProduct(prods[i]); 
            }
        }
        private void GetPhoto()
        {
            for (int i = 0; i < 4; i++)
            {
                using (WebClient wc = new WebClient())
                {
                    Stream stream = wc.OpenRead("https://" + jObj["media"]["images"][i]["url"].ToString() + "?$XXL$&wid=2000&fit=constrain");
                    Image img = Image.FromStream(stream);
                    Bitmap bit = new Bitmap(img);
                    bit.Save(@"C:\Users\gazalo\OneDrive\Desktop\SALES\" + sku + "-" + i + ".jpg", ImageFormat.Jpeg);
                }
            }
            string[] pictureList = { @"C:\Users\gazalo\OneDrive\Desktop\SALES\" + sku + "-0.jpg", @"C:\Users\gazalo\OneDrive\Desktop\SALES\" + sku + "-1.jpg", @"C:\Users\gazalo\OneDrive\Desktop\SALES\" + sku + "-2.jpg", @"C:\Users\gazalo\OneDrive\Desktop\SALES\" + sku + "-3.jpg" };
            eBayPictureService pictureService = new eBayPictureService(Context);
            picURLs = pictureService.UpLoadPictureFiles(PhotoDisplayCodeType.SuperSize, pictureList);

        }
        private string Removelink(string data)
        {
            int indexoflink = data.IndexOf("<a");
            while (indexoflink != -1)
            {
                int indexofendlink = data.IndexOf(">", indexoflink);
                data = data.Remove(indexoflink, indexofendlink - indexoflink + 1);
                data = data.Remove(data.IndexOf("</a>"), 4);
                indexoflink = data.IndexOf("<a");
            }
            indexoflink = data.IndexOf("<strong>");
            while (indexoflink != -1)
            {
                data = data.Remove(indexoflink, 8);
                data = data.Remove(data.IndexOf("</strong>"), 9);
                indexoflink = data.IndexOf("<strong>");
            }
            indexoflink = data.IndexOf("<ul");
            if (indexoflink != -1)
            {
                string uldata = " style=\"list - style: none; padding: 0px; margin: 0px 0px 10px; border: 0px; font: inherit; vertical - align: baseline; width: 299.859px; \"";
                data = data.Insert(indexoflink + 3, uldata);
            }
            indexoflink = data.IndexOf("<li");
            while (indexoflink != -1)
            {
                string lidata = " style=\"list - style: disc; padding: 0px 0px 0px 4px; margin: 0px 0px 0px 20px; border: 0px; font - style: inherit; font - variant: inherit; font - weight: inherit; font - stretch: inherit; font - size: inherit; line - height: 26px; font - family: inherit; vertical - align: baseline; \"";
                data = data.Insert(indexoflink + 3, lidata);
                indexoflink = data.IndexOf("<li", indexoflink + 1);
            }
            indexoflink = data.IndexOf("<div");
            while (indexoflink != -1)
            {
                string didata = " style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical - align: baseline; \"";
                data = data.Insert(indexoflink + 4, didata);
                indexoflink = data.IndexOf("<div", indexoflink + 1);
            }
            return data;
        }

        private void GetDescription()
        {
            description = defaulttext[0]+ Removelink(Convert.ToString(jObj["description"])) + defaulttext[1] + Removelink(Convert.ToString(jObj["brand"]["description"]))+ defaulttext[2] + Removelink(Convert.ToString(jObj["info"]["sizeAndFit"])) + defaulttext[3] + Removelink(Convert.ToString(jObj["info"]["careInfo"])) + defaulttext[4] + Removelink(Convert.ToString(jObj["info"]["aboutMe"])) + defaulttext[5];
        }
        private void Getcategory()
        {
            GetSuggestedCategoriesCall get = new GetSuggestedCategoriesCall(Context)
            {
                Query = Convert.ToString(jObj["name"]) +" " + Convert.ToString(jObj["gender"])
            };
            get.Execute();
            if (get.SuggestedCategoryList != null)
            {
                category = get.SuggestedCategoryList[0].Category.CategoryID;
            }
        }

        private void EbayApi(ref ApiContext Context)
        {
            Context = AppSettingHelper.GetApiContext();
            Context.ApiCredential.ApiAccount.Application = "gazalota-scraper-PRD-c786bc793-401b3c31";
            Context.ApiCredential.ApiAccount.Developer = "b9e821aa-ddcd-4a02-bcc5-7ced6af0e79a";
            Context.ApiCredential.ApiAccount.Certificate = "PRD-786bc793b135-1e88-48b0-b258-2668";
            Context.ApiCredential.eBayToken = "AgAAAA**AQAAAA**aAAAAA**whuSXA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ANk4OnCJCKoQudj6x9nY+seQ**OTwEAA**AAMAAA**yPZRzfIPhEPLV1Q0q1zSioCi+ndi5HyMB9SGHsGpNLi6/GaFPvgr+xIDn57ohGsO1bdBwtTkKW8L2OVTHHjKvabMbTCijwlaNz75ZeQX4TOk/D1ypYSXlSDhnf3yVWOk3C/7G8gPFqkEX6Yn8cLPgkl7/nM58EaM5EoMjGP8FcxJHeup6hOMFIZqkpELJkUCLnWZ3to3mdkf9fkHs7OGD5+5afIquL/V2Rhgb/VZWgz/+x9MaHGhsG27La2roTW+7HrNRQdp0DbLq8NUWjPQi1SMrPZkrgsgf/cazgOn83tXqS+g54CyyafXETpF6tZqqphOeiOLadfiBm5PzZMTv5G4mvQOrUuI9129YLsV6dxP/4u/lw9E8vH4PsgUcM9taN4daThQO9rOF/CaK32Rg5Q4kkFkjYDT8yRNpqLLaVfbNuQkFhLgsNsT4iY2pabLsoJDsz1v/ZXCRZhrT4UfhPQ1IdQJSKEJKZsS/8TfVgd6fN4TNTwF5VO5vFTCQfbccDuYsAiftz3b0Bmf7Wanc5m31vTcenwECkbVlEi0SP53MhtejRyDfq5R2l9D/aCJJ3XtC6hz4aP5AmveNjMWpbfIM/Q7RksBf0vEdyUwYLGtxdqPkZpYsYROUyV8bDfrY8Kh+uX+UCcCiz+tRa0+xeL+lPbDqBffzMXVtumID0Llm5DBYXBASbftCrFieLdUgXLBBVjwE6fbjPNTjPJyXV5bCSI4cF/Z4tdzRBLznPoC2koPKvwvJxgMc+IBNT3d";

            switch (site)
            {
                case 1:
                    Context.ErrorLanguage = ErrorLanguageCodeType.en_AU;
                    Context.Site = eBay.Service.Core.Soap.SiteCodeType.Australia;
                    break;
            }
            Context.Version = "1027";
            Context.Timeout = 120000;
            Context.SoapApiServerUrl = "https://api.ebay.com/wsapi";
            Context.SignInUrl = "https://signin.ebay.com/ws/eBayISAPI.dll?SignIn";
            Context.EPSServerUrl = "https://api.ebay.com/ws/api.dll";

            CallRetry oCallRetry = new CallRetry()
            {
                DelayTime = 1,
                MaximumRetries = 3
            };
            StringCollection oErrorCodes = new StringCollection
            {
                "10007",
                "2",
                "251"
            };
            oCallRetry.TriggerErrorCodes = oErrorCodes;
            TypeCollection oExceptions = new TypeCollection
            {
                typeof(System.Net.ProtocolViolationException),
                typeof(SdkException)
            };
            oCallRetry.TriggerExceptions = oExceptions;
            Context.CallRetry = oCallRetry;
        }

        private JObject AsosConnect(int sku)
        {
            bool problem = false;
            HttpResponseMessage response;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");
            string JSON;
            do
            {
                try
                {
                    response = client.GetAsync($"http://api.asos.com/product/catalogue/v2/products/{sku}?store={country}&lang={lang}&sizeSchema={country}&currency={currency}").Result;
                    JSON = response.Content.ReadAsStringAsync().Result;
                    return (JObject)JsonConvert.DeserializeObject(JSON);
                }
                catch (Exception ex)
                {
                    tele.Telegoogle(ex);
                    Thread.Sleep(5000);
                }
            } while (!problem);

            response = client.GetAsync($"http://api.asos.com/product/catalogue/v2/products/{sku}?store={country}&lang={lang}&sizeSchema={country}&currency={currency}").Result;
            JSON = response.Content.ReadAsStringAsync().Result;
            return (JObject)JsonConvert.DeserializeObject(JSON);
        }
    }

}
